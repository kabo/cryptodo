// @flow

import React, { Component } from 'react'
import {connect} from 'react-redux'
import Input from 'antd/lib/input'
import Row from 'antd/lib/row'
import Col from 'antd/lib/col'
import Button from 'antd/lib/button'
import uuidv4 from 'uuid/v4'
import {withRouter} from 'react-router-dom'
import { setNewTaskKeyId, setNewTaskTitle, addTask } from '../actions'
import KeySelector from './KeySelector'
import type { RouterHistory } from 'react-router-dom'
import type { Actions, Task, KeyMaterialState, KeyStoreState, SetNewTaskTitleAction, SetNewTaskKeyIdAction, AddTaskAction } from '../types'

type Props = {
  newTaskTitle: string,
  newTaskKeyId: string,
  keyStore: KeyStoreState,
  keyMaterial: KeyMaterialState,
  setNewTaskTitle: (newTaskTitle: string) => SetNewTaskTitleAction,
  setNewTaskKeyId: (newTaskKeyId: string) => SetNewTaskKeyIdAction,
  addTask: (task: Task, key: string) => AddTaskAction,
  history: RouterHistory,
}

class AddTask extends Component<Props> {
  componentDidMount() {
    const { keyMaterial, newTaskKeyId, setNewTaskKeyId, history } = this.props
    const actualKeyMaterial = keyMaterial.filter(k => !!k.key)
    if (actualKeyMaterial.length < 1) {
      history.push('/a/keys')
      return
    }
    if (!newTaskKeyId) {
      setNewTaskKeyId(actualKeyMaterial[0].keyId)
    }
  }
  addTask() {
    const { newTaskKeyId, newTaskTitle, keyMaterial } = this.props
    const chosenKey = keyMaterial.find(k => k.keyId === newTaskKeyId)
    if (!chosenKey) { return }
    // validation
    if (newTaskTitle.length < 1) { return }
    
    const key = chosenKey.key
    const newTask: Task = {
      taskId: uuidv4(),
      keyId: newTaskKeyId,
      title: newTaskTitle,
      completed: false,
      type: 'TASK'
    }
    this.props.setNewTaskTitle('')
    this.props.addTask(newTask, key)
    //persistTask(newTask)
  }
  render() {
    const { keyStore, newTaskTitle, newTaskKeyId } = this.props
    const addTask = this.addTask.bind(this)

    return (
      <div className="AddTask">
        <Row gutter={{xs: 8, sm: 8, lg: 16}}>
          <Col xs={{span: 11, offset: 3}} sm={{span: 14, offset: 2}} lg={{span: 17, offset: 1}}>
            <Input onPressEnter={addTask} value={newTaskTitle} onChange={(e) => this.props.setNewTaskTitle(e.target.value)} placeholder="New task" />
          </Col>
          <Col xs={6} sm={6} lg={4}>
            <KeySelector
              value={newTaskKeyId}
              onChange={this.props.setNewTaskKeyId}
              keys={keyStore}
            />
          </Col>
          <Col xs={0} sm={0} lg={2}>
            <Button icon="plus-circle-o" style={{width: '100%'}} onClick={addTask}>Add</Button>
          </Col>
          <Col xs={4} sm={2} lg={0}>
            <Button icon="plus-circle-o" style={{width: '100%'}} onClick={addTask}></Button>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = function(state) {
  return {
    newTaskTitle: state.ui.newTask.title,
    newTaskKeyId: state.ui.newTask.keyId,
    keyStore: state.crypto.keyStore,
    keyMaterial: state.crypto.keyMaterial,
  }
}

const mapDispatchToProps = function(dispatch: (Actions) => {}) {
  return {
    setNewTaskTitle: newTaskTitle => dispatch(setNewTaskTitle(newTaskTitle)),
    setNewTaskKeyId: newTaskKeyId => dispatch(setNewTaskKeyId(newTaskKeyId)),
    addTask: (task, key) => dispatch(addTask(task, key)),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddTask))



