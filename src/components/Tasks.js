// @flow

import React, { Component } from 'react'
import {connect} from 'react-redux'
import Row from 'antd/lib/row'
import Col from 'antd/lib/col'
import Icon from 'antd/lib/icon'
import Dropdown from 'antd/lib/dropdown'
import Menu from 'antd/lib/menu'
import Button from 'antd/lib/button'
import DraggableList from 'react-draggable-list'
import { loadTasks } from '../actions'
import Task from './Task'
import AddTask from './AddTask'
import type { Actions, EncryptedTasksState, TasksState, ShowCompletedValue, LoadTasksAction } from '../types'

type Props = {
  tasks: TasksState,
  textFilter: string,
  encryptedTasks: EncryptedTasksState,
  showCompleted: ShowCompletedValue,
  loadTasks: () => LoadTasksAction,
}

class Tasks extends Component<Props> {
  componentDidMount() {
    this.props.loadTasks()
  }
  render() {
    const { showCompleted, textFilter } = this.props
    const textFilterLower = textFilter.toLowerCase()
    const menu = (
      <Menu onClick={({key}) => this.props.loadTasks()}>
        <Menu.Item key="reload"><Icon type="sync" /> Reload tasks</Menu.Item>
      </Menu>
    )
    const tasks  = this.props.tasks.filter((task) => (showCompleted === 'all')
      || (showCompleted === 'completed' && task.completed)
      || (showCompleted === 'active' && !task.completed)
    )
      .filter(task => {
        if (!textFilter) { return true }
        return task.title.toLowerCase().includes(textFilterLower)
      })
      .sort((a, b) => a.title.toLowerCase().localeCompare(b.title.toLowerCase()))
    const taskIds = tasks.map(t => t.taskId)
    let allTasks: Array<any> = tasks
    if (showCompleted === 'all') {
      allTasks = tasks.concat(this.props.encryptedTasks.filter(t => !taskIds.includes(t.taskId)))
    }
    return (
      <div className="Tasks">
        <div style={{marginBottom: '-2.3em' /* position: absolute messes up the dropdown :/ */}}>
          <Dropdown overlay={menu} trigger={['hover', 'click']} placement="bottomLeft">
            <Button style={{width: '33px', padding: '0', zIndex: '3'}}>
              <Icon type="bars" />
            </Button>
          </Dropdown>
        </div>
        <Row style={{marginBottom: '32px'}}>
          <Col span={24}>
            <AddTask />
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <DraggableList
              list={allTasks}
              template={Task}
              itemKey="taskId"
            />
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = function(state) {
  return {
    tasks: state.crypto.tasks,
    encryptedTasks: state.crypto.encryptedTasks,
    showCompleted: state.ui.taskFilter.showCompleted,
    textFilter: state.ui.taskFilter.textFilter,
  }
}

const mapDispatchToProps = function(dispatch: (Actions) => {}) {
  return {
    loadTasks: () => dispatch(loadTasks()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Tasks)


