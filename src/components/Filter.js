// @flow

import React, { Component } from 'react'
import {connect} from 'react-redux'
import Button from 'antd/lib/button'
import Input from 'antd/lib/input'
import Row from 'antd/lib/row'
import { setShowCompleted, setFilterText } from '../actions'
import type { SetFilterTextAction, Actions, ShowCompletedValue, SetShowCompletedAction } from '../types'

type Props = {
  showCompleted: ShowCompletedValue,
  setShowCompleted: (showCompleted: ShowCompletedValue) => SetShowCompletedAction,
  setFilterText: (text: string) => SetFilterTextAction,
  textFilter: string,
}

class Filter extends Component<Props> {
  render() {
    const { showCompleted, setShowCompleted, textFilter, setFilterText } = this.props
    return (
      <div className="Filter" style={{marginLeft: '-24px'}}>
        <Row>
          <div className="filter-button-group-wrapper">
            <Button.Group className="filter-button-group">
              <Button className={`filter-button ${showCompleted==='active'?'filter-button-selected':''}`} onClick={() => setShowCompleted('active')} type="primary">Active</Button>
              <Button className={`filter-button ${showCompleted==='completed'?'filter-button-selected':''}`} onClick={() => setShowCompleted('completed')} type="primary">Completed</Button>
              <Button className={`filter-button ${showCompleted==='all'?'filter-button-selected':''}`} onClick={() => setShowCompleted('all')} type="primary">All</Button>
            </Button.Group>
          </div>
        </Row>
        <Row>
          <Input placeholder="Filter..." style={{width: '100%'}} value={textFilter} onChange={(e) => setFilterText(e.target.value)} />
        </Row>
      </div>
    )
  }
}

const mapStateToProps = function(state) {
  return {
    showCompleted: state.ui.taskFilter.showCompleted,
    textFilter: state.ui.taskFilter.textFilter,
  }
}

const mapDispatchToProps = function(dispatch: (Actions) => {}) {
  return {
    setShowCompleted: (showCompleted) => dispatch(setShowCompleted(showCompleted)),
    setFilterText: (textFilter) => dispatch(setFilterText(textFilter)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Filter)


