// @flow

import React, { Component } from 'react'
import Select from 'antd/lib/select'
import type { KeyStoreState } from '../types'
const { Option, OptGroup } = Select

type Props = {
  value: string,
  keys: KeyStoreState,
  onChange: (newTaskKeyId: string) => mixed,
}
export default class KeySelector extends Component<Props> {
  render() {
    const { value, onChange, keys } = this.props
    return (
      <Select
        value={value}
        onChange={onChange}
        dropdownMatchSelectWidth={false}
        style={{ width: '100%' }}
      >
        <OptGroup label="Select encryption key">
          {keys.map(key => <Option value={key.keyId} key={key.keyId}>{key.keyName}</Option>)}
        </OptGroup>
      </Select>
    )
  }
}
