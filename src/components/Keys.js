// @flow

import React, { Component } from 'react'
import {connect} from 'react-redux'
import Table from 'antd/lib/table'
import Button from 'antd/lib/button'
import Modal from 'antd/lib/modal'
import Row from 'antd/lib/row'
import Col from 'antd/lib/col'
import Input from 'antd/lib/input'
import Tooltip from 'antd/lib/tooltip'
import Icon from 'antd/lib/icon'
import InlineEdit from 'react-edit-inplace'
import nacl from 'tweetnacl'
import {encode as base64encode} from '@stablelib/base64'
import uuidv4 from 'uuid/v4'
import { Logger } from 'aws-amplify'
import { deleteKey, addKey, updateKey, setKeyKey, setAskDeleteKey, setShowNewKeyModal, cancelNewKeyModal, setNewKeyKey, setNewKeyName } from '../actions'
import type { Actions, Key, FullKey, KeyStoreState, KeyMaterialState, SetKeyKeyAction, SetAskDeleteKeyAction, SetShowNewKeyModalAction, CancelNewKeyModalAction, UpdateKeyAction, SetNewKeyKeyAction, SetNewKeyNameAction, AddKeyAction, DeleteKeyAction } from '../types'

const logger = new Logger('components/Task', 'INFO')

type Props = {
  keyStore: KeyStoreState,
  keyMaterial: KeyMaterialState,
  showNewKeyModal: boolean,
  askDeleteKey: string,
  newKeyName: string,
  newKeyKey: string,
  setKeyKey: (keyId: string, key: string) => SetKeyKeyAction,
  setAskDeleteKey: (keyId: ?string) => SetAskDeleteKeyAction,
  setShowNewKeyModal: (show: boolean) => SetShowNewKeyModalAction,
  cancelNewKeyModal: () => CancelNewKeyModalAction,
  cancelDeleteKey: () => SetAskDeleteKeyAction,
  updateKey: (key: Key) => UpdateKeyAction,
  setNewKeyKey: (key: string) => SetNewKeyKeyAction,
  setNewKeyName: (name: string) => SetNewKeyNameAction,
  addKey: (key: FullKey) => AddKeyAction,
  deleteKey: (keyId: string) => DeleteKeyAction,
}

class Keys extends Component<Props> {
  addKey() {
    const { newKeyName, newKeyKey } = this.props
    // TODO: validate
    const key: FullKey = {
      keyName: newKeyName,
      key: newKeyKey,
      keyId: uuidv4(),
    }
    this.props.addKey(key)
    this.props.cancelNewKeyModal()
  }
  setKeyName(key, keyName) {
    logger.debug('setKeyName')
    const updatedKey = { ...key, keyName }
    logger.debug('existing key', key)
    logger.debug('updated key', updatedKey)
    this.props.updateKey(updatedKey)
  }
  render() {
    const columns = [
      {
        title: 'Name',
        dataIndex: 'keyName',
        width: '20%',
        render: (text, item) => <InlineEdit
          text={text}
          paramName="keyName"
          className="key-keyName"
          activeClassName="editing-key-keyName"
          change={({keyName}) => this.setKeyName(item, keyName)}
        />
      },
      {
        title: 'Key',
        dataIndex: 'key',
        width: '60%',
        render: (text, item) => {
          const keyMaterial = this.props.keyMaterial.find(key => item.keyId === key.keyId)
          const key = keyMaterial ? keyMaterial.key : ''
          return <InlineEdit
            text={key}
            paramName="key"
            className="key-key blurry"
            activeClassName="editing-key-key"
            change={({key}) => this.props.setKeyKey(item.keyId, key)}
          />
        }
      },
      {
        title: 'Operations',
        key: 'operations',
        width: '20%',
        render: (text, item) => <div>
          <Button icon="delete" onClick={() => this.props.setAskDeleteKey(item.keyId)}>Delete</Button>
        </div>
      },
    ]
    const keyLength = nacl.secretbox.keyLength
    let keyToDelete, keyToDeleteName
    const { keyStore, showNewKeyModal, askDeleteKey, newKeyKey, newKeyName } = this.props
    const dataSource = keyStore.map(key => ({ ...key, key: key.keyId }))
    if (askDeleteKey) {
      keyToDelete = keyStore.find(key => key.keyId === askDeleteKey)
      if (keyToDelete) {
        keyToDeleteName = keyToDelete.keyName
      }
    }
    const addKey = this.addKey.bind(this)
    return (
      <div className="Keys">
        <div style={{textAlign: 'right', marginBottom: '16px'}}>
          <Button size="large" icon="plus-circle-o" onClick={() => this.props.setShowNewKeyModal(true)}>New key</Button>
        </div>
        <Table
          columns={columns}
          dataSource={dataSource}
        />
        <Modal
          visible={showNewKeyModal}
          title="Create new key"
          width={800}
          onOk={addKey}
          onCancel={this.props.cancelNewKeyModal}
          footer={(
            <div>
              <Button onClick={this.props.cancelNewKeyModal}>Cancel</Button>
              <Button onClick={addKey} type="primary" disabled={newKeyName.length < 1 || newKeyKey.length < 1}>Create key</Button>
            </div>
          )}
        >
          <div>
            <p>Your key will not be stored with CrypToDo. As a convenience it'll be stored in your browser, but you need to save and store your key safely. If you log in with a new browser the key won't be there and you'll be required to provide it again.</p>
            <Row style={{marginBottom: '8px'}}>
              <Col span={8}>
                Key name (unencrypted) <Tooltip title="This is the key name you'll see. It will be stored in the backend, unencrypted."><Icon type="info-circle" /></Tooltip>: 
              </Col>
              <Col span={16}>
                <Input value={newKeyName} onChange={(e) => this.props.setNewKeyName(e.target.value)} />
              </Col>
            </Row>
            <Row>
              <Col span={8}>
                Key ({keyLength} bytes, base64 encoded) <Tooltip title={`It is recommended that you just click the button to generate a random key. You can however input your own key. It has to be the correct number of bytes and then base64 encoded. NOT a base64 encoded string of length ${keyLength} characters!`}><Icon type="info-circle" /></Tooltip>: 
              </Col>
              <Col span={5}>
                <Button type="primary" onClick={() => this.props.setNewKeyKey(base64encode(nacl.randomBytes(keyLength)))}>Generate random</Button>
              </Col>
              <Col span={11}>
                <Input value={newKeyKey} onChange={(e) => this.props.setNewKeyKey(e.target.value)} />
              </Col>
              {/* TODO: offer scrypt-async-js method of generating key, storing salt in the db, allows users to type in a passphrase */}
            </Row>
          </div>
        </Modal>
        <Modal
          visible={!!askDeleteKey}
          title="Delete key"
          onOk={() => { this.props.deleteKey(askDeleteKey); this.props.setAskDeleteKey(null)}}
          onCancel={this.props.cancelDeleteKey}
        >
          Are you sure you want to delete the key "{keyToDeleteName}"? You won't be able to access any tasks encrypted with this key. There is no undo.
        </Modal>
      </div>
    )
  }
}

const mapStateToProps = function(state) {
  return {
    keyStore: state.crypto.keyStore,
    keyMaterial: state.crypto.keyMaterial,
    showNewKeyModal: state.ui.key.showNewKeyModal,
    askDeleteKey: state.ui.key.askDeleteKey,
    newKeyName: state.ui.key.newKeyName,
    newKeyKey: state.ui.key.newKeyKey,
  }
}

const mapDispatchToProps = function(dispatch: (Actions) => {}) {
  return {
    setKeyKey: (keyId, key) => dispatch(setKeyKey(keyId, key)),
    setAskDeleteKey: (keyId) => dispatch(setAskDeleteKey(keyId)),
    setShowNewKeyModal: (show) => dispatch(setShowNewKeyModal(show)),
    cancelNewKeyModal: () => dispatch(cancelNewKeyModal()),
    cancelDeleteKey: () => dispatch(setAskDeleteKey(null)),
    updateKey: key => dispatch(updateKey(key)),
    setNewKeyKey: (key) => dispatch(setNewKeyKey(key)),
    setNewKeyName: (name) => dispatch(setNewKeyName(name)),
    addKey: (key) => dispatch(addKey(key)),
    deleteKey: (keyId) => dispatch(deleteKey(keyId)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Keys)

