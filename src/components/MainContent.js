// @flow

import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import { Logger } from 'aws-amplify'
import sizeMe from 'react-sizeme'
import Loadable from 'react-loadable'
import Loading from '../Loading'

const logger = new Logger('MainContent', 'INFO')

type Props = {
  size: {
    width: number
  }
}

const LoadableTasks = Loadable({
  loader: () => import('./Tasks'),
  loading: Loading
})
const LoadableKeys = Loadable({
  loader: () => import('./Keys'),
  loading: Loading
})
const LoadableAccount = Loadable({
  loader: () => import('./Account'),
  loading: Loading
})

class MainContent extends Component<Props> {
  render() {
    if (this.props.size.width < 300) {
      logger.debug('width < 300')
      return (<div className="main-content-bg"></div>)
    }
    logger.debug('width >= 300')
    return (
      <div className="main-content-bg">
        <Route exact path="/a/tasks" component={LoadableTasks} />
        <Route exact path="/a/keys" component={LoadableKeys} />
        <Route exact path="/a/account" component={LoadableAccount} />
      </div>
    )
  }
}

export default sizeMe({refreshMode: 'debounce', refreshRate: 50})(MainContent)
