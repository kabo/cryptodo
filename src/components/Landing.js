// @flow

import React, { Component } from 'react'
import {withRouter} from 'react-router-dom'
import { Logger } from 'aws-amplify'
import Layout from 'antd/lib/layout'
import Button from 'antd/lib/button'
import '../landing.css'
import type { RouterHistory } from 'react-router-dom'

const { Content, Footer } = Layout
const logger = new Logger('Landing', 'INFO')

type Props = {
  history: RouterHistory,
}

class Landing extends Component<Props> {
  render() {
    logger.debug('landing page render')
    return (
      <div className="Landing">
        <Layout style={{ backgroundColor: 'transparent' }}>
          <Content>
            <div className="landing-logo">
              <img alt="[logo]" src="assets/logo2.png" style={{width: '100%'}} />
            </div>
            <div className="tagline">Zero-knowledge todo-list</div>
            <div className="account-buttons">
              <Button size="large" type="primary" onClick={() => this.props.history.push('/a/keys')}>Log in or sign up</Button>
            </div>
            <div className="blurb">
              <p>So, you like encrypted, zero-knowledge services. You're using <a href="https://tutanota.com/">Tutanota</a> instead of gmail, <a href="https://cryptpad.fr/">CryptPad</a> instead of gdrive, and <a href="https://spideroak.com/">SpiderOak</a> or <a href="https://www.tarsnap.com/">Tarsnap</a> for your backups. But what about your todo-list?</p>
              <p>CrypToDo is a zero-knowledge todo-list, your todo items get encrypted in your browser before they're sent to the backend, making it impossible for anyone but you to read them.</p>
            </div>
            <div className="faq">
              <div className="faq-heading">FAQ</div>
              <dl>
                <dt>How much does it cost?</dt>
                <dd>It's free.</dd>

                <dt>How are my todo items encrypted?</dt>
                <dd>Before you begin you have to generate a key. The key never gets sent to the backend. If you log in using a different browser you will have to provide the key again. Once you have a key you can start creating and completing tasks. The title and the status (task completed or not) are encrypted using <a href="https://tweetnacl.js.org/">TweetNaCl</a> Secretbox (xsalsa20-poly1305) before the encrypted data is sent to the backend together with the key id (not the key!).</dd>

                <dt>I'm missing feature X or I've found a bug</dt>
                <dd>Please <a href="https://gitlab.com/kabo/cryptodo/issues">create an issue</a>.</dd>

                <dt>How can I trust you? Can I see the code?</dt>
                <dd>You don't have to trust me, you can see the code in <a href="https://gitlab.com/kabo/cryptodo">my GitLab repo</a>. The code is released under the <a href="http://coil.apotheon.org/">COIL (Copyfree Open Innovation License) 0.5</a> license. Feel free to run the service in your own AWS account!</dd>

                <dt>How do you make money from this?</dt>
                <dd>I don't, this is currently a personal hobby project. If you feel this should be more than that, feel free to <a href="https://www.linkedin.com/in/callekabo/">reach out to me on LinkedIn</a>.</dd>
              </dl>
            </div>
          </Content>
          <Footer style={{ backgroundColor: 'transparent' }}>
            <div className="footer-disclaimer">Terms of service: This service is provided as is, with no warranty or guarantee, expressed or implied. Do not rely on it for privacy or anonymity. IN NO EVENT SHALL THE AUTHORS, ASSEMBLERS, OR HOLDERS OF COPYRIGHT OR OTHER LEGAL PRIVILEGE BE LIABLE FOR ANY CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT, OR OTHERWISE ARISING FROM, OUT OF, OR IN CONNECTION WITH THE WORK OR THE USE OF OR OTHER DEALINGS IN THE WORK.
              <br />
            Do not eat. Batteries not included. Beware of dog. Do not use while operating a motor vehicle or heavy equipment. Keep away from sunlight, pets, and small children. Slippery when wet.</div>
          </Footer>
        </Layout>
      </div>
    )
  }
}

export default withRouter(Landing)

