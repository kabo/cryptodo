// @flow

import React, { Component } from 'react'
import './Checkbox.css'

function dec2hex(dec: number): string {
  return ('0' + dec.toString(16)).substr(-2)
}

function generateId(len?: number): string {
  var arr = new Uint8Array((len || 40) / 2)
  window.crypto.getRandomValues(arr)
  return Array.from(arr, dec2hex).join('')
}

type Props = {
  size: number,
  checked: boolean,
  id?: string,
  onChange: () => mixed,
}

class Checkbox extends Component<Props> {
  render() {
    const size = this.props.size || 33
    const width = `${size}px`
    const height = `${size}px`
    const checked = this.props.checked || false
    const id = this.props.id || generateId()
    return (
      <div className="Checkbox" style={{
        width, 
        height,
      }}>
        <input
          type="checkbox"
          id={id}
          checked={checked}
          onChange={this.props.onChange}
        />
        <label htmlFor={id} style={{
          width,
          height,
        }} />
        <div className="tick" style={{
          width: `${size*2/3}px`,
          height: `${size*2/5}px`,
          borderWidth: `${size/10}px`,
          top: `${size/5}px`,
          left: `${size/6}px`,
        }}></div>
      </div>
    )
  }
}

export default Checkbox
