// @flow

import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {connect} from 'react-redux'
import { withAuthenticator } from 'aws-amplify-react'
import { Logger } from 'aws-amplify'
import AntLayout from 'antd/lib/layout'
import Menu from './Menu'
import MainContent from './MainContent'
import { setUserId, loadKeys } from '../actions'
import type { Actions, SetUserIdAction, LoadKeysAction } from '../types'

const logger = new Logger('Layout', 'INFO')
const { Header, Content, Sider } = AntLayout

type Props = {
  loadKeys: () => LoadKeysAction,
  setUserId: (id: string) => SetUserIdAction,
  authData: {
    username: string
  }
}

class Layout extends Component<Props> {
  componentDidMount() {
    const userId = this.props.authData.username
    logger.debug('userId', userId)
    this.props.setUserId(userId)
    this.props.loadKeys()
  }
  render() {
    return (
      <div className="Layout">
        <AntLayout>
          <Header>
            <div>
              <Link to="/">
                <img alt="[logo]" src="assets/logo2.png" style={{height: '64px'}} />
              </Link>
            </div>
          </Header>
          <AntLayout hasSider={true}>
            <Sider
              width={300}
              style={{height: '100vh', paddingTop: '16px'}}
              breakpoint="lg"
              collapsedWidth="0"
              defaultCollapsed={true}
            >
              <Menu />
            </Sider>
            <Content className="main-content">
              <MainContent />
            </Content>
          </AntLayout>
        </AntLayout>
      </div>
    )
  }
}

const mapStateToProps = function(state) {
  return {
  }
}

const mapDispatchToProps = function(dispatch: (Actions) => {}) {
  return {
    loadKeys: () => dispatch(loadKeys()),
    setUserId: (id) => dispatch(setUserId(id)),
  }
}

export default withAuthenticator(connect(mapStateToProps, mapDispatchToProps)(Layout))


