// @flow

import React, { Component } from 'react'
import {connect} from 'react-redux'
import Row from 'antd/lib/row'
import Col from 'antd/lib/col'
// import Icon from 'antd/lib/icon'
import Checkbox from './Checkbox'
import Button from 'antd/lib/button'
import Popconfirm from 'antd/lib/popconfirm'
import InlineEdit from 'react-edit-inplace'
import { Logger } from 'aws-amplify'
import { deleteTask, updateTask } from '../actions'
import KeySelector from './KeySelector'
import type { Actions, Task, UpdateTaskAction, DeleteTaskAction, KeyStoreState, KeyMaterialState } from '../types'

const logger = new Logger('components/Task', 'INFO')

type Props = {
  updateTask: (task: Task, key: string) => UpdateTaskAction,
  deleteTask: (taskId: string) => DeleteTaskAction,
  keyStore: KeyStoreState,
  keyMaterial: KeyMaterialState,
  item: Task,
  dragHandle: () => mixed,
}

class CTask extends Component<Props> {
  changeKey(task, keyId) {
    logger.debug('changeKey')
    const keyMaterial = this.props.keyMaterial.find(k => k.keyId === keyId)
    if (!keyMaterial || !keyMaterial.key) { return }
    const updatedTask = { ...task, keyId }
    logger.debug('existing task', task)
    logger.debug('updated task', updatedTask)
    this.props.updateTask(updatedTask, keyMaterial.key)
  }
  toggleTaskCompleted(task) {
    logger.debug('toggleTaskCompleted')
    const keyMaterial = this.props.keyMaterial.find(k => k.keyId === task.keyId)
    if (!keyMaterial || !keyMaterial.key) { return }
    const updatedTask = { ...task, completed: !task.completed }
    logger.debug('existing task', task)
    logger.debug('updated task', updatedTask)
    this.props.updateTask(updatedTask, keyMaterial.key)
  }
  setTaskTitle(task, title) {
    logger.debug('setTaskTitle')
    const keyMaterial = this.props.keyMaterial.find(k => k.keyId === task.keyId)
    if (!keyMaterial || !keyMaterial.key) { return }
    const updatedTask = { ...task, title }
    logger.debug('existing task', task)
    logger.debug('updated task', updatedTask)
    this.props.updateTask(updatedTask, keyMaterial.key)
  }
  render() {
    const { keyStore, item /*, dragHandle */ } = this.props
    if (item.type === 'ENCRYPTED_TASK') {
      return (
        <div className="Task">
          <Row gutter={{xs: 8, sm: 8, lg: 16}}>
            <Col xs={24} sm={22} lg={22}>
              Unable to decrypt task
            </Col>
            <Col xs={0} sm={0} lg={2}>
              <Popconfirm title="Delete task?" okText="Delete" cancelText="Cancel" onConfirm={() => this.props.deleteTask(item.taskId)}>
                <Button icon="delete" style={{width: '100%'}}>Delete</Button>
              </Popconfirm>
            </Col>
            <Col xs={0} sm={2} lg={0}>
              <Popconfirm title="Delete task?" okText="Delete" cancelText="Cancel" onConfirm={() => this.props.deleteTask(item.taskId)}>
                <Button icon="delete" style={{width: '100%'}}></Button>
              </Popconfirm>
            </Col>
          </Row>
        </div>
      )
    }
    return (
      <div className="Task">
        <Row gutter={{xs: 8, sm: 8, lg: 16}}>
          {/*<Col span={1}>
            {dragHandle(<Icon className="taskDragger" type="swap" style={{transform: 'rotate(90deg)'}} />)}
          </Col>*/}
          <Col xs={3} sm={2} lg={1}>
            <Checkbox size={33} className="task-checkbox" checked={item.completed} onChange={() => this.toggleTaskCompleted.bind(this)(item)} />
          </Col>
          <Col xs={21} sm={20} lg={17}>
            <InlineEdit
              text={item.title}
              paramName="title"
              className="task-title"
              activeClassName="task-title-edited"
              change={({title}) => this.setTaskTitle.bind(this)(item, title)}
            />
          </Col>
          <Col xs={0} sm={0} lg={4}>
            <KeySelector
              value={item.keyId}
              onChange={(keyId) => this.changeKey.bind(this)(item, keyId)}
              keys={keyStore}
            />
          </Col>
          <Col xs={0} sm={0} lg={2}>
            <Popconfirm title="Delete task?" okText="Delete" cancelText="Cancel" onConfirm={() => this.props.deleteTask(item.taskId)}>
              <Button icon="delete" style={{width: '100%'}}>Delete</Button>
            </Popconfirm>
          </Col>
          <Col xs={0} sm={2} lg={0}>
            <Popconfirm title="Delete task?" okText="Delete" cancelText="Cancel" onConfirm={() => this.props.deleteTask(item.taskId)}>
              <Button icon="delete" style={{width: '100%'}}></Button>
            </Popconfirm>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = function(state) {
  return {
    keyStore: state.crypto.keyStore,
    keyMaterial: state.crypto.keyMaterial,
  }
}

const mapDispatchToProps = function(dispatch: (Actions) => {}) {
  return {
    updateTask: (task, key) => dispatch(updateTask(task, key)),
    deleteTask: taskId => dispatch(deleteTask(taskId)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CTask)



