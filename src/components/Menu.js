// @flow

import React, { Component } from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import { Logger, Auth } from 'aws-amplify'
import Menu from 'antd/lib/menu'
import Icon from 'antd/lib/icon'
import Filter from './Filter'
import type { RouterHistory, Location } from 'react-router-dom'
import type { KeyMaterialState } from '../types'

const logger = new Logger('Menu', 'INFO')

type Props = {
  location: Location,
  history: RouterHistory,
  keyMaterial: KeyMaterialState,
}

class SideMenu extends Component<Props> {
  menuClick({item, key, keyPath}) {
    switch (key) {
    case 'logout':
      Auth.signOut()
        .then(() => {
          window.location.reload()
        })
        .catch(err => logger.err(err))
      break
    case 'filter':
      break
    default:
      this.props.history.push(`/a/${key}`)
      break
    }
  }
  render() {
    const menuClick = this.menuClick.bind(this)
    const selectedKey = /\/(\w+)$/.exec(this.props.location.pathname)
    const selectedKeys = [selectedKey ? selectedKey[1] : null]
    const openKeys = selectedKeys[0] === 'tasks' ? ['tasks'] : []
    const { keyMaterial } = this.props
    const actualKeyMaterial = keyMaterial.filter(k => !!k.key)
    const hasUserKeys = actualKeyMaterial.length > 0
    return (
      <div className="SideMenu">
        <Menu
          mode="inline"
          theme="dark"
          selectedKeys={selectedKeys}
          onClick={menuClick}
          openKeys={openKeys}
        >
          <Menu.SubMenu
            key="tasks"
            disabled={!hasUserKeys}
            title={<span><Icon type="check-square-o" /> <span>Tasks</span></span>}
            className={openKeys.includes('tasks') ? 'submenu submenu-selected' : 'submenu'}
            onTitleClick={() => menuClick({key: 'tasks'})}
          >
            <Menu.Item key="filter" style={{height: '100%'}}>
              <Filter />
            </Menu.Item>
          </Menu.SubMenu>
          <Menu.Item key="keys"><Icon type="key" /> <span>Keys</span></Menu.Item>
          <Menu.Item key="account"><Icon type="user" /> <span>Account</span></Menu.Item>
          <Menu.Item key="logout"><Icon type="logout" /> <span>Log out</span></Menu.Item>
        </Menu>
      </div>
    )
  }
}

const mapStateToProps = function(state) {
  return {
    keyMaterial: state.crypto.keyMaterial,
  }
}

const mapDispatchToProps = function(dispatch) {
  return {
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(SideMenu))

