// @flow

import React, { Component } from 'react'
import { Logger } from 'aws-amplify'
import {connect} from 'react-redux'
import Card from 'antd/lib/card'
import Button from 'antd/lib/button'
import Row from 'antd/lib/row'
import Col from 'antd/lib/col'
import Input from 'antd/lib/input'
//import Modal from 'antd/lib/modal'
import notification from 'antd/lib/notification'
import { setChangePasswordError, setChangePasswordSuccess, changePassword, setCurrentPassword, setNewPassword, setConfirmNewPassword } from '../actions'
import type { Actions, SetConfirmNewPasswordAction, SetChangePasswordSuccessAction, SetChangePasswordErrorAction, SetNewPasswordAction, ChangePasswordAction, SetCurrentPasswordAction } from '../types'

const logger = new Logger('components/Account', 'INFO')

type Props = {
    currentPassword: string,
    newPassword: string,
    confirmNewPassword: string,
    changePasswordError: string,
    changePasswordSuccess: boolean,
    changingPassword: boolean,
    changePassword: (oldPassword: string, newPassword: string) => ChangePasswordAction,
    setCurrentPassword: (pwd: string) => SetCurrentPasswordAction,
    setNewPassword: (pwd: string) => SetNewPasswordAction,
    setConfirmNewPassword: (pwd: string) => SetConfirmNewPasswordAction,
    setChangePasswordError: (error: string) => SetChangePasswordErrorAction,
    setChangePasswordSuccess: (value: boolean) => SetChangePasswordSuccessAction,
}

class Account extends Component<Props> {
  componentDidUpdate(prevProps, prevState) {
    if (this.props.changePasswordError) {
      this.props.setChangePasswordError('')
    }
    if (this.props.changePasswordSuccess) {
      this.props.setChangePasswordSuccess(false)
    }
  }
  changePassword() {
    logger.debug('Changing password')
    const {currentPassword, newPassword, confirmNewPassword} = this.props
    const enableSend = currentPassword.length > 0 && newPassword.length > 0 && confirmNewPassword === newPassword
    if (!enableSend) { return }
    this.props.changePassword(currentPassword, newPassword)
  }
  render() {
    const {currentPassword, newPassword, confirmNewPassword, changingPassword, changePasswordError, changePasswordSuccess } = this.props
    const enableButton = currentPassword.length > 0 && newPassword.length > 0 && confirmNewPassword === newPassword
    if (changePasswordError) {
      notification.error({
        message: 'An error occured',
        description: changePasswordError,
        duration: null,
      })
    }
    if (changePasswordSuccess) {
      notification.success({
        message: 'Password changed!',
      })
    }
    return (
      <div className="Account">
        <Card title="Change password" style={{width: '500px', marginTop: '50px'}}>
          <Row gutter={16}>
            <Col span="8">
              <label htmlFor="account-current-password" style={{lineHeight: '3em'}}>Current password</label>
            </Col>
            <Col span="16">
              <Input type="password" onPressEnter={this.changePassword.bind(this)} value={currentPassword} onChange={e => this.props.setCurrentPassword(e.target.value)} id="account-current-password" size="large" />
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span="8">
              <label htmlFor="account-new-password" style={{lineHeight: '3em'}}>New password</label>
            </Col>
            <Col span="16">
              <Input type="password" onPressEnter={this.changePassword} value={newPassword} onChange={e => this.props.setNewPassword(e.target.value)} id="account-new-password" size="large" />
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span="8">
              <label htmlFor="account-new-password-confirm" style={{lineHeight: '3em'}}>Repeat new password</label>
            </Col>
            <Col span="16">
              <Input type="password" onPressEnter={this.changePassword} value={confirmNewPassword} onChange={e => this.props.setConfirmNewPassword(e.target.value)} id="account-new-password-confirm" size="large" />
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span="16" offset="8">
              <Button onClick={this.changePassword} type="primary" size="large" loading={changingPassword} disabled={!enableButton}>Change password</Button>
            </Col>
          </Row>
        </Card>
        {/*
        <Card title="Delete account" style={{width: '500px', marginTop: '50px'}}>
          <div style={{textAlign: 'center'}}>
            <Button type="danger" size="large" onClick={() => this.props.setAskDeleteAccount(true)}>Delete account</Button>
          </div>
        </Card>
        <Modal
          title="Really delete account?"
          visible={this.props.askDeleteAccount}
          onOk={this.props.deleteAccount}
          onCancel={() => this.props.setAskDeleteAccount(false)}
        >
          Are you sure you want to permanently delete your account?
          {/* TODO: ask for current password?
        </Modal>
        */}
      </div>
    )
  }
}

const mapStateToProps = function(state) {
  return {
    currentPassword: state.ui.account.currentPassword,
    newPassword: state.ui.account.newPassword,
    confirmNewPassword: state.ui.account.confirmNewPassword,
    changePasswordError: state.ui.account.changePasswordError,
    changePasswordSuccess: state.ui.account.changePasswordSuccess,
    changingPassword: state.ui.loads.changingPassword,
    //askDeleteAccount: state.ui.account.askDeleteAccount,
  }
}

const mapDispatchToProps = function(dispatch: (Actions) => {}) {
  return {
    changePassword: (oldPassword, newPassword) => dispatch(changePassword(oldPassword, newPassword)),
    //deleteAccount: () => dispatch(deleteAccount()),
    //setAskDeleteAccount: value => dispatch(setAskDeleteAccount(value)),
    setCurrentPassword: pwd => dispatch(setCurrentPassword(pwd)),
    setNewPassword: pwd => dispatch(setNewPassword(pwd)),
    setConfirmNewPassword: pwd => dispatch(setConfirmNewPassword(pwd)),
    setChangePasswordError: error => dispatch(setChangePasswordError(error)),
    setChangePasswordSuccess: value => dispatch(setChangePasswordSuccess(value)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Account)

