// @flow

import { Logger } from 'aws-amplify'
import type { UiState, UiActions } from '../types'

const logger = new Logger('reducers/ui', 'INFO')
const defaultState = {
  newTask: {
    title: '',
    keyId: null,
  },
  loads: {
    tasksLoading: false,
    addingTask: false,
    deletingTask: null,
    changingPassword: false,
  },
  taskFilter: {
    showCompleted: 'active',
    textFilter: '',
  },
  key: {
    showNewKeyModal: false,
    askDeleteKey: null,
    newKeyKey: '',
    newKeyName: '',
  },
  account: {
    currentPassword: '',
    newPassword: '',
    confirmNewPassword: '',
    askDeleteAccount: false,
    changePasswordError: '',
    changePasswordSuccess: false,
  }
}

const ui = function(state: UiState = defaultState, action: UiActions): UiState {
  logger.debug('action', action)
  switch (action.type) {
  case 'SET_NEW_TASK':
    return { ...state, newTask: action.newTask }
  case 'SET_NEW_TASK_TITLE':
    return { ...state, newTask: { ...state.newTask, title: action.title } }
  case 'SET_NEW_TASK_KEY_ID':
    return { ...state, newTask: { ...state.newTask, keyId: action.keyId } }
  case 'LOAD_KEYS_REQUEST':
    return { ...state, loads: { ...state.loads, userKeys: true } }
  case 'LOAD_KEYS_DONE':
    return { ...state, loads: { ...state.loads, userKeys: false } }
  case 'LOAD_KEYS_FAILED':
    return { ...state, loads: { ...state.loads, userKeys: false } }
  case 'SET_USER_KEYS_LOADING':
    return { ...state, loads: { ...state.loads, userKeys: action.loading } }
  case 'SET_TASKS_LOADING':
    return { ...state, loads: { ...state.loads, tasks: action.loading } }
  case 'SET_NEW_KEY_KEY':
    return { ...state, key: { ...state.key, newKeyKey: action.key } }
  case 'SET_NEW_KEY_NAME':
    return { ...state, key: { ...state.key, newKeyName: action.name } }
  case 'CANCEL_NEW_KEY_MODAL':
    return { ...state, key: { ...state.key, showNewKeyModal: false, newKeyName: '', newKeyKey: '' } }
  case 'SET_SHOW_NEW_KEY_MODAL':
    return { ...state, key: { ...state.key, showNewKeyModal: action.show } }
  case 'SET_ASK_DELETE_KEY':
    return { ...state, key: { ...state.key, askDeleteKey: action.keyId } }
  case 'SET_TASKFILTER':
    return { ...state, taskFilter: action.filter }
  case 'SET_TASKFILTER_SHOW_COMPLETED':
    return { ...state, taskFilter: { ...state.taskFilter, showCompleted: action.showCompleted } }
  case 'SET_TASKFILTER_TEXT':
    return { ...state, taskFilter: { ...state.taskFilter, textFilter: action.textFilter } }
    //case 'SET_ASK_DELETE_ACCOUNT':
    //  return { ...state, account: { ...state.account, askDeleteAccount: action.ask } }
  case 'SET_CURRENT_PASSWORD':
    return { ...state, account: { ...state.account, currentPassword: action.currentPassword } }
  case 'SET_NEW_PASSWORD':
    return { ...state, account: { ...state.account, newPassword: action.newPassword } }
  case 'SET_CONFIRM_NEW_PASSWORD':
    return { ...state, account: { ...state.account, confirmNewPassword: action.confirmNewPassword } }
  case 'SET_CHANGE_PASSWORD_ERROR':
    return { ...state, account: { ...state.account, changePasswordError: action.error } }
  case 'SET_CHANGE_PASSWORD_SUCCESS':
    return { ...state, account: { ...state.account, changePasswordSuccess: action.value } }
  case 'CHANGE_PASSWORD_REQUEST':
    return { ...state, loads: { ...state.loads, changingPassword: true } }
  case 'CHANGE_PASSWORD_COMMIT':
    return {
      ...state,
      loads: { ...state.loads, changingPassword: false },
      account: {
        ...state.account,
        changePasswordError: '',
        currentPassword: '',
        newPassword: '',
        confirmNewPassword: '',
        changePasswordSuccess: true,
      },
    }
  case 'CHANGE_PASSWORD_ROLLBACK':
    //logger.error('CHANGE_PASSWORD_ROLLBACK action', action)
    return {
      ...state,
      loads: { ...state.loads, changingPassword: false },
      account: {
        ...state.account,
        changePasswordError: action.payload ? action.payload.message : 'Unknown error',
        changePasswordSuccess: false,
      },
    }
  default:
    return state
  }
}

export default ui


