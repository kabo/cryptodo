// @flow

import { Logger } from 'aws-amplify'
import type { UserState, UserActions } from '../types'

const logger = new Logger('reducers/user', 'INFO')
const defaultState = { id: null }

const user = function(state: UserState = defaultState, action: UserActions): UserState {
  logger.debug('action', action)
  switch (action.type) {
  case 'SET_USER_ID':
    return { ...state, id: action.id }
  default:
    return state
  }
}

export default user

