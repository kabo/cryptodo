// @flow

import { Logger } from 'aws-amplify'
import { decryptTask } from '../crypto.js'
import type { LoadKeysDoneAction, LoadTasksDoneAction, AddTaskAction, AddTaskRollbackAction, UpdateTaskAction, UpdateTaskCommitAction, UpdateTaskRollbackAction, DeleteTaskAction, DeleteTaskRollbackAction, DeleteTaskCommitAction, UpdateKeyAction, UpdateKeyCommitAction, UpdateKeyRollbackAction, SetKeyKeyAction, AddKeyAction, AddKeyRollbackAction, DeleteKeyAction, DeleteKeyCommitAction, DeleteKeyRollbackAction, TasksState, EncryptedTasksState, Task, CryptoState, CryptoActions  } from '../types'

const logger = new Logger('reducers/crypto', 'INFO')
const defaultState = {
  tasks: [],
  encryptedTasks: [],
  keyStore: [],
  keyMaterial: [],
  changedKeyStore: [],
  changedKeyMaterial: [],
  changedTasks: [],
  changedEncryptedTasks: [],
}

const actions = {
  'LOAD_TASKS_DONE': (state: CryptoState, action: LoadTasksDoneAction) => {
    const encryptedTasks: EncryptedTasksState = action.payload.map(task => ({ ...task, type: 'ENCRYPTED_TASK' }))
    const tasks: TasksState = []
    encryptedTasks.forEach(encryptedTask => {
      const keyMaterial = state.keyMaterial.find(key => key.keyId === encryptedTask.keyId)
      if (!keyMaterial || !keyMaterial.key) { return }
      try {
        const task: Task =  decryptTask(encryptedTask, keyMaterial.key)
        tasks.push(task)
      } catch (err) {
        logger.warn('unable to decrypt task', encryptedTask)
      }
    })
    return {
      ...state,
      encryptedTasks,
      tasks,
    }
  },
  'ADD_TASK_REQUEST': (state: CryptoState, action: AddTaskAction) => {
    return { ...state, tasks: [ ...state.tasks, action.task ] }
  },
  'ADD_TASK_ROLLBACK': (state: CryptoState, action: AddTaskRollbackAction) => {
    return { ...state, tasks: state.tasks.filter(task => task.taskId !== action.taskId) }
  },
  'UPDATE_TASK_REQUEST': (state: CryptoState, action: UpdateTaskAction) => {
    const changedTasks = [ ...state.changedTasks ]
    const task = state.tasks.find(task => task.taskId === action.task.taskId)
    if (task) {
      changedTasks.push(task)
    }
    return {
      ...state,
      changedTasks,
      tasks: state.tasks.map(task => (task.taskId === action.task.taskId) ? action.task : task)
    }
  },
  'UPDATE_TASK_COMMIT': (state: CryptoState, action: UpdateTaskCommitAction) => {
    return {
      ...state,
      changedTasks: state.changedTasks.filter(task => task.taskId !== action.taskId),
    }
  },
  'UPDATE_TASK_ROLLBACK': (state: CryptoState, action: UpdateTaskRollbackAction) => {
    const changedTaskEntry = state.changedTasks.find(ct => ct.taskId === action.taskId)
    if (!changedTaskEntry) {
      logger.error('UPDATE_TASK_ROLLBACK: unable to find task in changedTasks, unable to perform rollback')
      return state
    }
    return {
      ...state,
      changedTasks: state.changedTasks.filter(task => task.taskId !== action.taskId),
      tasks: state.tasks.map(task => (task.taskId === action.taskId) ? changedTaskEntry : task)
    }
  },
  'DELETE_TASK_REQUEST': (state: CryptoState, action: DeleteTaskAction) => {
    const taskEntry = state.tasks.find(task => task.taskId === action.taskId)
    let changedTasks
    if (!taskEntry) {
      logger.warn('DELETE_TASK_REQUEST: unable to find task in tasks')
      changedTasks = [ ...state.changedTasks ]
    } else {
      changedTasks = [ ...state.changedTasks, taskEntry ]
    }
    const encryptedTaskEntry = state.encryptedTasks.find(task => task.taskId === action.taskId)
    let changedEncryptedTasks
    if (!encryptedTaskEntry) {
      logger.warn('DELETE_TASK_REQUEST: unable to find task in encryptedTasks')
      changedEncryptedTasks = [ ...state.changedEncryptedTasks ]
    } else {
      changedEncryptedTasks = [ ...state.changedEncryptedTasks, encryptedTaskEntry ]
    }
    return {
      ...state,
      changedTasks,
      changedEncryptedTasks,
      tasks: state.tasks.filter(task => task.taskId !== action.taskId),
      encryptedTasks: state.encryptedTasks.filter(task => task.taskId !== action.taskId),
    }
  },
  'DELETE_TASK_COMMIT': (state: CryptoState, action: DeleteTaskCommitAction) => {
    return {
      ...state,
      changedEncryptedTasks: state.changedEncryptedTasks.filter(task => task.taskId === action.taskId),
      changedTasks: state.changedTasks.filter(task => task.taskId === action.taskId),
    }
  },
  'DELETE_TASK_ROLLBACK': (state: CryptoState, action: DeleteTaskRollbackAction) => {
    const changedTaskEntry = state.changedTasks.find(task => task.taskId === action.taskId)
    let tasks
    if (!changedTaskEntry) {
      logger.warn('DELETE_TASK_ROLLBACK: unable to find task in changedTasks')
      tasks = [ ...state.tasks ]
    } else {
      tasks = [ ...state.tasks, changedTaskEntry ]
    }
    const changedEncryptedTaskEntry = state.changedEncryptedTasks.find(task => task.taskId === action.taskId)
    let encryptedTasks
    if (!changedEncryptedTaskEntry) {
      logger.warn('DELETE_TASK_ROLLBACK: unable to find task in changedTasks')
      encryptedTasks = [ ...state.encryptedTasks ]
    } else {
      encryptedTasks = [ ...state.encryptedTasks, changedEncryptedTaskEntry ]
    }
    return {
      ...state,
      tasks,
      encryptedTasks,
      changedTasks: state.changedTasks.filter(task => task.taskId === action.taskId),
      changedEncryptedTasks: state.changedEncryptedTasks.filter(task => task.taskId === action.taskId),
    }
  },
  'LOAD_KEYS_DONE': (state: CryptoState, action: LoadKeysDoneAction) => {
    return {
      ...state,
      keyStore: action.payload,
      keyMaterial: action.payload.map(key => {
        const existingKey = state.keyMaterial.find(k => k.keyId === key.keyId)
        return { keyId: key.keyId, key: (existingKey ? existingKey.key : '') }
      }),
    }
  },
  'UPDATE_KEY_REQUEST': (state: CryptoState, action: UpdateKeyAction) => {
    const keyStoreEntry = state.keyStore.find(ck => ck.keyId === action.key.keyId)
    if (!keyStoreEntry) {
      logger.error('UPDATE_KEY_REQUEST: unable to find key in keyStore, unable to update state')
      return state
    }
    return {
      ...state,
      changedKeyStore: [ ...state.changedKeyStore, keyStoreEntry ],
      keyStore: state.keyStore.map(key => (key.keyId === action.key.keyId) ? action.key : key)
    }
  },
  'UPDATE_KEY_COMMIT': (state: CryptoState, action: UpdateKeyCommitAction) => {
    return {
      ...state,
      changedKeyStore: state.changedKeyStore.filter(key => key.keyId !== action.keyId),
    }
  },
  'UPDATE_KEY_ROLLBACK': (state: CryptoState, action: UpdateKeyRollbackAction) => {
    const changedKeyStoreEntry = state.changedKeyStore.find(ck => ck.keyId === action.keyId)
    if (!changedKeyStoreEntry) {
      logger.error('UPDATE_KEY_ROLLBACK: Unable to find key in changedKeyStore, unable to roll back!')
      return state
    }
    return {
      ...state,
      changedKeyStore: state.changedKeyStore.filter(key => key.keyId !== action.keyId),
      keyStore: state.keyStore.map(key => (key.keyId === action.keyId) ? changedKeyStoreEntry : key)
    }
  },
  'SET_KEY_KEY': (state: CryptoState, action: SetKeyKeyAction) => {
    return { ...state, keyMaterial: state.keyMaterial.map(key => (key.keyId === action.keyId) ? { ...key , key: action.key } : key) }
  },
  'ADD_KEY_REQUEST': (state: CryptoState, action: AddKeyAction) => {
    return {
      ...state,
      keyStore: [ ...state.keyStore, action.forKeyStore ],
      keyMaterial: [ ...state.keyMaterial, action.forKeyMaterial ],
    }
  },
  'ADD_KEY_ROLLBACK': (state: CryptoState, action: AddKeyRollbackAction) => {
    return {
      ...state,
      keyStore: state.keyStore.filter(key => key.keyId !== action.keyId),
      keyMaterial: state.keyMaterial.filter(key => key.keyId !== action.keyId),
    }
  },
  'DELETE_KEY_REQUEST': (state: CryptoState, action: DeleteKeyAction) => {
    const newChangedKeyStoreEntry = state.keyStore.find(key => key.keyId === action.keyId)
    const newChangedKeyMaterialEntry = state.keyMaterial.find(key => key.keyId === action.keyId)
    const changedKeyStore = newChangedKeyStoreEntry ? [ ...state.changedKeyStore, newChangedKeyStoreEntry ] : state.keyStore
    const changedKeyMaterial = newChangedKeyMaterialEntry ? [ ...state.changedKeyMaterial, newChangedKeyMaterialEntry ] : state.keyMaterial
    return {
      ...state,
      keyStore: state.keyStore.filter(key => key.keyId !== action.keyId),
      keyMaterial: state.keyMaterial.filter(key => key.keyId !== action.keyId),
      changedKeyStore,
      changedKeyMaterial,
    }
  },
  'DELETE_KEY_COMMIT': (state: CryptoState, action: DeleteKeyCommitAction) => {
    return {
      ...state,
      changedKeyStore: state.changedKeyStore.filter(key => key.keyId !== action.keyId),
      changedKeyMaterial: state.changedKeyMaterial.filter(key => key.keyId !== action.keyId),
    }
  },
  'DELETE_KEY_ROLLBACK': (state: CryptoState, action: DeleteKeyRollbackAction) => {
    const newKeyStoreEntry = state.changedKeyStore.find(key => key.keyId === action.keyId)
    const newKeyMaterialEntry = state.changedKeyMaterial.find(key => key.keyId === action.keyId)
    const keyStore = newKeyStoreEntry ? [ ...state.keyStore, newKeyStoreEntry ] : state.keyStore
    const keyMaterial = newKeyMaterialEntry ? [ ...state.keyMaterial, newKeyMaterialEntry ] : state.keyMaterial
    return {
      ...state,
      keyStore,
      keyMaterial,
      changedKeyStore: state.changedKeyStore.filter(key => key.keyId !== action.keyId),
      changedKeyMaterial: state.changedKeyMaterial.filter(key => key.keyId !== action.keyId),
    }
  },
}

const crypto = function(state: CryptoState = defaultState, action: CryptoActions): CryptoState {
  logger.debug('action:', action)
  switch (action.type) {
  case 'LOAD_TASKS_DONE':
    return actions[action.type](state, action)
  case 'ADD_TASK_REQUEST':
    return actions[action.type](state, action)
  case 'ADD_TASK_ROLLBACK':
    return actions[action.type](state, action)
  case 'UPDATE_TASK_REQUEST':
    return actions[action.type](state, action)
  case 'UPDATE_TASK_COMMIT':
    return actions[action.type](state, action)
  case 'UPDATE_TASK_ROLLBACK':
    return actions[action.type](state, action)
  case 'DELETE_TASK_REQUEST':
    return actions[action.type](state, action)
  case 'DELETE_TASK_COMMIT':
    return actions[action.type](state, action)
  case 'DELETE_TASK_ROLLBACK':
    return actions[action.type](state, action)
  case 'LOAD_KEYS_DONE':
    return actions[action.type](state, action)
  case 'UPDATE_KEY_REQUEST':
    return actions[action.type](state, action)
  case 'UPDATE_KEY_COMMIT':
    return actions[action.type](state, action)
  case 'UPDATE_KEY_ROLLBACK':
    return actions[action.type](state, action)
  case 'SET_KEY_KEY':
    return actions[action.type](state, action)
  case 'ADD_KEY_REQUEST':
    return actions[action.type](state, action)
  case 'ADD_KEY_ROLLBACK':
    return actions[action.type](state, action)
  case 'DELETE_KEY_REQUEST':
    return actions[action.type](state, action)
  case 'DELETE_KEY_COMMIT':
    return actions[action.type](state, action)
  case 'DELETE_KEY_ROLLBACK':
    return actions[action.type](state, action)
  default:
    return state
  }
}

export default crypto

