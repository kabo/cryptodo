// @flow

import { combineReducers } from 'redux'
import crypto from './crypto'
import user from './user'
import ui from './ui'

export default combineReducers({
  crypto,
  user,
  ui,
})

