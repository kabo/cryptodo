// @flow

export type EncryptedTask = {
  taskId: string,
  keyId: string,
  nonce: string,
  data: string,
  type: 'ENCRYPTED_TASK',
}

export type Task = {
  taskId: string,
  keyId: string,
  title: string,
  completed: boolean,
  type: 'TASK',
  //sortorder: number,
}

export type Key = {
  keyId: string,
  keyName: string,
  key?: string,
  // salt: string // if using scrypt and passphrase
}

export type FullKey = {
  keyId: string,
  keyName: string,
  key: string,
  // salt: string // if using scrypt and passphrase
}

export type KeyStoreEntry = {
  keyId: string,
  keyName: string,
}
export type KeyStoreState = Array<KeyStoreEntry>

export type KeyMaterialEntry = {
  keyId: string,
  key: string,
}
export type KeyMaterialState = Array<KeyMaterialEntry>

export type ShowCompletedValue = 'active' | 'all' | 'completed'

export type TasksState = Array<Task>

export type EncryptedTasksState = Array<EncryptedTask>

export type UserState = {
  +id: ?string,
}

export type NewTaskState = {
  +title: string,
  +keyId: ?string,
}

export type FilterState = {
  +showCompleted: ShowCompletedValue,
  +textFilter: string,
}

export type LoadsState = {
  +tasksLoading: boolean,
  +addingTask: boolean,
  +deletingTask: ?string,
  +changingPassword: boolean,
}

export type KeyState = {
  +showNewKeyModal: boolean,
  +askDeleteKey: ?string,
  +newKeyName: string,
  +newKeyKey: string,
}
export type AccountState = {
  currentPassword: string,
  newPassword: string,
  confirmNewPassword: string,
  askDeleteAccount: boolean,
  changePasswordError: string,
  changePasswordSuccess: boolean,
}

export type UiState = {
  +newTask: NewTaskState,
  +loads: LoadsState,
  +taskFilter: FilterState,
  +key: KeyState,
  +account: AccountState,
  // add keyfilter, showNewKeyModal, askDeleteKey here
}

export type CryptoState = {
  +tasks: TasksState,
  +encryptedTasks: EncryptedTasksState,
  +keyStore: KeyStoreState,
  +keyMaterial: KeyMaterialState,
  +changedKeyStore: KeyStoreState,
  +changedKeyMaterial: KeyMaterialState,
  +changedTasks: TasksState,
  +changedEncryptedTasks: EncryptedTasksState,
}

export type State = {
  +crypto: CryptoState,
  +user: UserState,
  +ui: UiState,
}

export type AuthEffect = {
  authAction: string,
  payload: {
    oldPassword: string,
    newPassword: string,
  },
  type: 'AUTH_EFFECT',
}
export type ApiEffect = {
  apiName: string,
  path: string,
  method: string,
  payload?: mixed,
  type: 'API_EFFECT',
}
export type Effects = ApiEffect | AuthEffect

export type SetTasksAction = { type: 'SET_TASKS', tasks: TasksState }
export type SetUserKeysAction = { type: 'SET_USER_KEYS', keys: Array<Key> }
export type SetNewTaskAction = { type: 'SET_NEW_TASK', newTask: NewTaskState }
export type SetFilterAction = { type: 'SET_TASKFILTER', filter: FilterState }
export type SetTasksLoadingAction = { type: 'SET_TASKS_LOADING', loading: boolean }
export type SetUserKeysLoadingAction = { type: 'SET_USER_KEYS_LOADING', loading: boolean }
export type SetShowCompletedAction = { type: 'SET_TASKFILTER_SHOW_COMPLETED', showCompleted: ShowCompletedValue }
export type SetFilterTextAction = { type: 'SET_TASKFILTER_TEXT', textFilter: string }
export type SetNewTaskTitleAction = { type: 'SET_NEW_TASK_TITLE', title: string }
export type SetNewTaskKeyIdAction = { type: 'SET_NEW_TASK_KEY_ID', keyId: string }
export type SetTaskTitleAction = { type: 'SET_TASK_TITLE', taskId: string, title: string }
export type SetTaskKeyIdAction = { type: 'SET_TASK_KEY_ID', taskId: string, keyId: string }
export type AddTaskCommitAction = { type: 'ADD_TASK_COMMIT', taskId: string }
export type AddTaskRollbackAction = { type: 'ADD_TASK_ROLLBACK', taskId: string }
export type AddTaskAction = {
  type: 'ADD_TASK_REQUEST',
  task: Task,
  meta: {
    offline: {
      effect: ApiEffect,
      commit: AddTaskCommitAction,
      rollback: AddTaskRollbackAction,
    }
  }
}
export type ToggleTaskCompletedAction = { type: 'TOGGLE_TASK_COMPLETED', taskId: string }
export type SetKeyNameAction = { type: 'SET_KEY_NAME', keyId: string, name: string }
export type SetKeyKeyAction = { type: 'SET_KEY_KEY', keyId: string, key: string }
export type SetNewKeyKeyAction = { type: 'SET_NEW_KEY_KEY', key: string }
export type SetNewKeyNameAction = { type: 'SET_NEW_KEY_NAME', name: string }
export type CancelNewKeyModalAction = { type: 'CANCEL_NEW_KEY_MODAL' }
export type AddKeyCommitAction = { type: 'ADD_KEY_COMMIT', keyId: string }
export type AddKeyRollbackAction = { type: 'ADD_KEY_ROLLBACK', keyId: string }
export type AddKeyAction = {
  type: 'ADD_KEY_REQUEST',
  forKeyStore: KeyStoreEntry,
  forKeyMaterial: KeyMaterialEntry,
  meta: {
    offline: {
      effect: ApiEffect,
      commit: AddKeyCommitAction,
      rollback: AddKeyRollbackAction,
    }
  }
}
export type DeleteTaskCommitAction = { type: 'DELETE_TASK_COMMIT', taskId: string }
export type DeleteTaskRollbackAction = { type: 'DELETE_TASK_ROLLBACK', taskId: string }
export type DeleteTaskAction = {
  type: 'DELETE_TASK_REQUEST',
  taskId: string,
  meta: {
    offline: {
      effect: ApiEffect,
      commit: DeleteTaskCommitAction,
      rollback: DeleteTaskRollbackAction,
    }
  }
}
export type SetUserIdAction = { type: 'SET_USER_ID', id: string }
export type SetCurrentPasswordAction = { type: 'SET_CURRENT_PASSWORD', currentPassword: string }
export type SetNewPasswordAction = { type: 'SET_NEW_PASSWORD', newPassword: string }
export type SetConfirmNewPasswordAction = { type: 'SET_CONFIRM_NEW_PASSWORD', confirmNewPassword: string }
export type SetChangePasswordErrorAction = { type: 'SET_CHANGE_PASSWORD_ERROR', error: string }
export type SetChangePasswordSuccessAction = { type: 'SET_CHANGE_PASSWORD_SUCCESS', value: boolean }
export type SetShowNewKeyModalAction = { type: 'SET_SHOW_NEW_KEY_MODAL', show: boolean }
export type SetAskDeleteKeyAction = { type: 'SET_ASK_DELETE_KEY', keyId: ?string }
export type UpdateTaskCommitAction = { type: 'UPDATE_TASK_COMMIT', taskId: string }
export type UpdateTaskRollbackAction = { type: 'UPDATE_TASK_ROLLBACK', taskId: string }
export type UpdateTaskAction = {
  type: 'UPDATE_TASK_REQUEST',
  task: Task,
  meta: {
    offline: {
      effect: ApiEffect,
      commit: UpdateTaskCommitAction,
      rollback: UpdateTaskRollbackAction,
    }
  }
}
export type UpdateKeyCommitAction = { type: 'UPDATE_KEY_COMMIT', keyId: string }
export type UpdateKeyRollbackAction = { type: 'UPDATE_KEY_ROLLBACK', keyId: string }
export type UpdateKeyAction = {
  type: 'UPDATE_KEY_REQUEST',
  key: Key,
  meta: {
    offline: {
      effect: ApiEffect,
      commit: UpdateKeyCommitAction,
      rollback: UpdateKeyRollbackAction,
    }
  }
}
export type LoadTasksDoneAction = { type: 'LOAD_TASKS_DONE', payload: Array<EncryptedTask> }
export type LoadTasksFailedAction = { type: 'LOAD_TASKS_FAILED', payload: {message: string} }
export type LoadTasksAction = {
  type: 'LOAD_TASKS_REQUEST',
  meta: {
    offline: {
      effect: ApiEffect,
      commit: { type: 'LOAD_TASKS_DONE' },
      rollback: { type: 'LOAD_TASKS_FAILED' },
    }
  }
}
export type LoadKeysDoneAction = { type: 'LOAD_KEYS_DONE', payload: KeyStoreState }
export type LoadKeysFailedAction = { type: 'LOAD_KEYS_FAILED' }
export type LoadKeysAction = {
  type: 'LOAD_KEYS_REQUEST',
  meta: {
    offline: {
      effect: ApiEffect,
      commit: { type: 'LOAD_KEYS_DONE' },
      rollback: LoadKeysFailedAction,
    }
  }
}
export type ChangePasswordCommitAction = { type: 'CHANGE_PASSWORD_COMMIT' }
export type ChangePasswordRollbackAction = { type: 'CHANGE_PASSWORD_ROLLBACK', payload?: {message: string} }
export type ChangePasswordAction = {
  type: 'CHANGE_PASSWORD_REQUEST',
  meta: {
    offline: {
      effect: AuthEffect,
      commit: ChangePasswordCommitAction,
      rollback: ChangePasswordRollbackAction,
    }
  }
}
export type DeleteKeyCommitAction = { type: 'DELETE_KEY_COMMIT', keyId: string }
export type DeleteKeyRollbackAction = { type: 'DELETE_KEY_ROLLBACK', keyId: string }
export type DeleteKeyAction = {
  type: 'DELETE_KEY_REQUEST',
  keyId: string,
  meta: {
    offline: {
      effect: ApiEffect,
      commit: DeleteKeyCommitAction,
      rollback: DeleteKeyRollbackAction,
    }
  }
}

export type UserActions = SetUserIdAction

export type UiActions = SetNewTaskAction | SetNewTaskTitleAction | SetNewTaskKeyIdAction | LoadKeysAction | SetUserKeysLoadingAction | SetTasksLoadingAction | LoadKeysDoneAction | LoadKeysFailedAction | SetNewKeyKeyAction | SetNewKeyNameAction | CancelNewKeyModalAction | SetShowNewKeyModalAction | SetAskDeleteKeyAction | SetFilterAction | SetShowCompletedAction | SetCurrentPasswordAction | SetNewPasswordAction | SetConfirmNewPasswordAction | SetChangePasswordErrorAction | SetChangePasswordSuccessAction | ChangePasswordAction | ChangePasswordCommitAction | ChangePasswordRollbackAction | SetFilterTextAction

export type CryptoActions = LoadTasksDoneAction | AddTaskAction | AddTaskRollbackAction | UpdateTaskAction | UpdateTaskCommitAction | UpdateTaskRollbackAction | DeleteTaskAction | DeleteTaskRollbackAction | DeleteTaskCommitAction | UpdateKeyAction | UpdateKeyCommitAction | UpdateKeyRollbackAction | SetKeyKeyAction | AddKeyAction | AddKeyRollbackAction | DeleteKeyAction | DeleteKeyCommitAction | DeleteKeyRollbackAction | LoadKeysDoneAction

export type Actions = UserActions | UiActions | CryptoActions | LoadTasksAction

// export type Action = SetTasksAction | SetNewTaskAction | SetFilterAction | SetTasksLoadingAction |  SetNewTaskTitleAction | AddTaskAction | ToggleTaskCompletedAction | SetShowCompletedAction | SetTaskTitleAction | SetKeyNameAction | SetKeyKeyAction | CancelNewKeyModalAction | SetNewKeyKeyAction | SetNewKeyNameAction | AddKeyAction | DeleteKeyAction | DeleteTaskAction | SetNewTaskKeyIdAction | SetTaskKeyIdAction | SetUserKeysLoadingAction | SetUserKeysAction | SetUserIdAction


