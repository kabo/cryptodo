// @flow

import React from 'react'
import { Route } from 'react-router-dom'
import Loadable from 'react-loadable'
import Loading from './Loading'

const LoadableLanding = Loadable({
  loader: () => import('./components/Landing'),
  loading: Loading
})

const LoadableLayout = Loadable({
  loader: () => import('./components/Layout'),
  loading: Loading
})

function App() {
  return (
    <div className="App">
      <Route exact path="/" component={LoadableLanding} />
      <Route path="/a" component={LoadableLayout} />
    </div>
  )
}

export default App

