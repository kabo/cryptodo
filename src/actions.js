// @flow

import type { FullKey, SetTaskTitleAction, CancelNewKeyModalAction, Task, DeleteKeyAction, UpdateTaskAction, UpdateKeyAction, LoadTasksAction, SetTasksLoadingAction, SetTasksAction, SetNewTaskAction, SetFilterAction, SetNewTaskTitleAction, AddTaskAction, SetShowCompletedAction, TasksState, NewTaskState, FilterState, ShowCompletedValue, SetKeyKeyAction, SetKeyNameAction, SetShowNewKeyModalAction, SetAskDeleteKeyAction, SetNewKeyNameAction, SetNewKeyKeyAction, AddKeyAction, Key, DeleteTaskAction, SetNewTaskKeyIdAction, SetTaskKeyIdAction, SetUserKeysAction, SetUserKeysLoadingAction, SetUserIdAction, LoadKeysAction, SetConfirmNewPasswordAction, SetChangePasswordErrorAction, SetChangePasswordSuccessAction, ChangePasswordAction, SetNewPasswordAction, SetCurrentPasswordAction, SetFilterTextAction } from './types'
import { encryptTask } from './crypto'

export function setUserKeysLoading(loading: boolean): SetUserKeysLoadingAction {
  return {
    type: 'SET_USER_KEYS_LOADING',
    loading
  }
}

export function setTasksLoading(loading: boolean): SetTasksLoadingAction {
  return {
    type: 'SET_TASKS_LOADING',
    loading
  }
}

export function setUserId(id: string): SetUserIdAction {
  return {
    type: 'SET_USER_ID',
    id
  }
}

export function setUserKeys(keys: Array<Key>): SetUserKeysAction {
  return {
    type: 'SET_USER_KEYS',
    keys
  }
}

export function setTasks(tasks: TasksState): SetTasksAction {
  return {
    type: 'SET_TASKS',
    tasks
  }
}

export function setNewTask(newTask: NewTaskState): SetNewTaskAction {
  return {
    type: 'SET_NEW_TASK',
    newTask
  }
}

export function setTaskFilter(filter: FilterState): SetFilterAction {
  return {
    type: 'SET_TASKFILTER',
    filter
  }
}

export function setNewTaskKeyId(keyId: string): SetNewTaskKeyIdAction {
  return {
    type: 'SET_NEW_TASK_KEY_ID',
    keyId
  }
}

export function setNewTaskTitle(title: string): SetNewTaskTitleAction {
  return {
    type: 'SET_NEW_TASK_TITLE',
    title
  }
}

export function setTaskKeyId(taskId: string, keyId: string): SetTaskKeyIdAction {
  return {
    type: 'SET_TASK_KEY_ID',
    keyId,
    taskId
  }
}

export function setTaskTitle(taskId: string, title: string): SetTaskTitleAction {
  return {
    type: 'SET_TASK_TITLE',
    title,
    taskId
  }
}

export function setShowCompleted(showCompleted: ShowCompletedValue): SetShowCompletedAction {
  return {
    type: 'SET_TASKFILTER_SHOW_COMPLETED',
    showCompleted
  }
}

export function setKeyName(keyId: string, name: string): SetKeyNameAction {
  return {
    type: 'SET_KEY_NAME',
    name,
    keyId
  }
}

export function setKeyKey(keyId: string, key: string): SetKeyKeyAction {
  return {
    type: 'SET_KEY_KEY',
    key,
    keyId
  }
}

export function setNewKeyKey(key: string): SetNewKeyKeyAction {
  return {
    type: 'SET_NEW_KEY_KEY',
    key
  }
}

export function setNewKeyName(name: string): SetNewKeyNameAction {
  return {
    type: 'SET_NEW_KEY_NAME',
    name
  }
}

export function setAskDeleteKey(keyId: ?string): SetAskDeleteKeyAction {
  return {
    type: 'SET_ASK_DELETE_KEY',
    keyId
  }
}

export function setShowNewKeyModal(show: boolean): SetShowNewKeyModalAction {
  return {
    type: 'SET_SHOW_NEW_KEY_MODAL',
    show
  }
}

export function cancelNewKeyModal(): CancelNewKeyModalAction {
  return {
    type: 'CANCEL_NEW_KEY_MODAL',
  }
}

export function addTask(task: Task, key: string): AddTaskAction {
  const { taskId } = task
  const encryptedTask = encryptTask(task, key)
  return {
    type: 'ADD_TASK_REQUEST',
    task,
    meta: {
      offline: {
        effect: { apiName: 'tasksCRUD', path: '/tasks', method: 'post', payload: encryptedTask, type: 'API_EFFECT' },
        commit: { type: 'ADD_TASK_COMMIT', taskId },
        rollback: { type: 'ADD_TASK_ROLLBACK', taskId },
      }
    }
  }
}

export function loadTasks(): LoadTasksAction {
  return {
    type: 'LOAD_TASKS_REQUEST',
    meta: {
      offline: {
        effect: { apiName: 'tasksCRUD', path: '/tasks', method: 'get', type: 'API_EFFECT' },
        commit: { type: 'LOAD_TASKS_DONE' },
        rollback: { type: 'LOAD_TASKS_FAILED' },
      }
    }
  }
}

export function deleteTask(taskId: string): DeleteTaskAction {
  return {
    type: 'DELETE_TASK_REQUEST',
    taskId,
    meta: {
      offline: {
        effect: { apiName: 'tasksCRUD', path: `/tasks/object/${taskId}`, method: 'del', type: 'API_EFFECT' },
        commit: { type: 'DELETE_TASK_COMMIT', taskId },
        rollback: { type: 'DELETE_TASK_ROLLBACK', taskId },
      }
    }
  }
}

export function addKey(key: FullKey): AddKeyAction {
  const { keyId } = key
  const forKeyStore = { keyId, keyName: key.keyName }
  const forKeyMaterial = { keyId, key: key.key }
  return {
    type: 'ADD_KEY_REQUEST',
    forKeyStore,
    forKeyMaterial,
    meta: {
      offline: {
        effect: { apiName: 'keysCRUD', path: '/keys', method: 'post', payload: forKeyStore, type: 'API_EFFECT' },
        commit: { type: 'ADD_KEY_COMMIT', keyId },
        rollback: { type: 'ADD_KEY_ROLLBACK', keyId },
      }
    }
  }
}

export function deleteKey(keyId: string): DeleteKeyAction {
  return {
    type: 'DELETE_KEY_REQUEST',
    keyId,
    meta: {
      offline: {
        effect: { apiName: 'keysCRUD', path: `/keys/object/${keyId}`, method: 'del', type: 'API_EFFECT' },
        commit: { type: 'DELETE_KEY_COMMIT', keyId },
        rollback: { type: 'DELETE_KEY_ROLLBACK', keyId },
      }
    }
  }
}

export function loadKeys(): LoadKeysAction {
  return {
    type: 'LOAD_KEYS_REQUEST',
    meta: {
      offline: {
        effect: { apiName: 'keysCRUD', path: '/keys', method: 'get', type: 'API_EFFECT' },
        commit: { type: 'LOAD_KEYS_DONE' },
        rollback: { type: 'LOAD_KEYS_FAILED' },
      }
    }
  }
}

export function updateTask(task: Task, key: string): UpdateTaskAction {
  const { taskId } = task
  const encryptedTask = encryptTask(task, key)
  return {
    type: 'UPDATE_TASK_REQUEST',
    task,
    meta: {
      offline: {
        effect: { apiName: 'tasksCRUD', path: `/tasks`, method: 'put', payload: encryptedTask, type: 'API_EFFECT' },
        commit: { type: 'UPDATE_TASK_COMMIT', taskId },
        rollback: { type: 'UPDATE_TASK_ROLLBACK', taskId },
      }
    }
  }
}

export function updateKey(key: Key): UpdateKeyAction {
  const { keyId } = key
  return {
    type: 'UPDATE_KEY_REQUEST',
    key,
    meta: {
      offline: {
        effect: { apiName: 'keysCRUD', path: `/keys`, method: 'put', payload: { keyId, keyName: key.keyName }, type: 'API_EFFECT' },
        commit: { type: 'UPDATE_KEY_COMMIT', keyId },
        rollback: { type: 'UPDATE_KEY_ROLLBACK', keyId },
      }
    }
  }
}

/*
export function setAskDeleteAccount(ask: boolean): SetAskDeleteAccountAction {
  return {
    type: 'SET_ASK_DELETE_ACCOUNT',
    ask
  }
}
*/

// TODO: implement deleteAccount
// deleteAccount, effect deletes tasks, keys, and account

export function changePassword(oldPassword: string, newPassword: string): ChangePasswordAction {
  return {
    type: 'CHANGE_PASSWORD_REQUEST',
    meta: {
      offline: {
        effect: { authAction: 'changePassword', payload: { oldPassword, newPassword }, type: 'AUTH_EFFECT'},
        commit: { type: 'CHANGE_PASSWORD_COMMIT' },
        rollback: { type: 'CHANGE_PASSWORD_ROLLBACK' },
      }
    }
  }
}

export function setCurrentPassword(currentPassword: string): SetCurrentPasswordAction {
  return {
    type: 'SET_CURRENT_PASSWORD',
    currentPassword
  }
}

export function setNewPassword(newPassword: string): SetNewPasswordAction {
  return {
    type: 'SET_NEW_PASSWORD',
    newPassword
  }
}

export function setConfirmNewPassword(confirmNewPassword: string): SetConfirmNewPasswordAction {
  return {
    type: 'SET_CONFIRM_NEW_PASSWORD',
    confirmNewPassword
  }
}

export function setChangePasswordError(error: string): SetChangePasswordErrorAction {
  return {
    type: 'SET_CHANGE_PASSWORD_ERROR',
    error
  }
}

export function setChangePasswordSuccess(value: boolean): SetChangePasswordSuccessAction {
  return {
    type: 'SET_CHANGE_PASSWORD_SUCCESS',
    value
  }
}

export function setFilterText(textFilter: string): SetFilterTextAction {
  return {
    type: 'SET_TASKFILTER_TEXT',
    textFilter
  }
}

