// @flow

import React from 'react'
import ReactDOM from 'react-dom'
import { HashRouter as Router } from 'react-router-dom'
import {createStore, applyMiddleware, compose} from 'redux'
import {Provider} from 'react-redux'
import ReduxAsyncQueue from 'redux-async-queue'
import { offline } from '@redux-offline/redux-offline'
import defaultOfflineConfig from '@redux-offline/redux-offline/lib/defaults'
import Amplify from 'aws-amplify'
import reconciler from './reconciler'
import aws_exports from './aws-exports'
import LocaleProvider from 'antd/lib/locale-provider'
import enUS from 'antd/lib/locale-provider/en_US'
import reducer from './reducers'
import 'antd/dist/antd.min.css'
import './index.css'
import App from './App'
import ErrorBoundary from './ErrorBoundary'
//import registerServiceWorker from './registerServiceWorker'

Amplify.configure(aws_exports)

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const offlineConfig = {
  ...defaultOfflineConfig,
  persistOptions: {
    whitelist: ['crypto']
  },
  effect: (effect, action) => {
    if (effect.url) { return defaultOfflineConfig.effect(effect, action) }
    return reconciler(effect)
  }
}
const store = createStore(reducer, composeEnhancers(applyMiddleware(ReduxAsyncQueue), offline(offlineConfig)))

const rootElement = document.getElementById('root')
if (rootElement) {
  ReactDOM.render(
    <ErrorBoundary>
      <Provider store={store}>
        <LocaleProvider locale={enUS}>
          <Router>
            <App />
          </Router>
        </LocaleProvider>
      </Provider>
    </ErrorBoundary>,
    rootElement)
  //registerServiceWorker()
} else {
  console.error('No root element found!')
}
