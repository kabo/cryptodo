// @flow

import { Logger } from 'aws-amplify'
import nacl from 'tweetnacl'
import {decode as utf8decode, encode as utf8encode} from '@stablelib/utf8'
import {decode as base64decode, encode as base64encode} from '@stablelib/base64'
import type { EncryptedTask, Task } from './types'

const logger = new Logger('crypto', 'INFO')

const decrypt = function({data, nonce, key}: {data: string, nonce: string, key: string}) {
  const udata = base64decode(data)
  const unonce = base64decode(nonce)
  const ukey = base64decode(key)
  const decrypted = nacl.secretbox.open(udata, unonce, ukey)
  return utf8decode(decrypted)
}

const encrypt = function({data, key}: {data: string, key: string}) {
  const udata = utf8encode(data)
  const ukey = base64decode(key)
  const unonce = nacl.randomBytes(nacl.secretbox.nonceLength)
  const encrypted = nacl.secretbox(udata, unonce, ukey)
  return { data: base64encode(encrypted), nonce: base64encode(unonce) }
}

export const decryptTask = function(encryptedTask: EncryptedTask, key: string): Task {
  logger.debug('decrypting task', encryptedTask)
  const { data, nonce } = encryptedTask
  const decrypted = decrypt({data, nonce, key})
  const taskData = JSON.parse(decrypted)
  return { ...encryptedTask, ...taskData, type: 'TASK' }
}

export const encryptTask = function(task: Task, key: string): EncryptedTask {
  logger.debug('encrypting task', task)
  const { taskId, keyId, title, completed } = task
  const toBeEncrypted = {
    title,
    completed
  }
  const { data, nonce } = encrypt({data: JSON.stringify(toBeEncrypted), key})
  return {
    taskId,
    keyId,
    nonce,
    data,
    type: 'ENCRYPTED_TASK'
  }
}

