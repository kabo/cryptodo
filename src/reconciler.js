// @flow

import { API, Auth, Logger } from 'aws-amplify'
import type { Effects, ApiEffect, AuthEffect } from './types'

const logger = new Logger('reconciler', 'INFO')

const apiFunction = function(effect: ApiEffect) {
  const { apiName, method, path, payload } = effect
  const init = {}
  if (payload) {
    init.body = payload
  }
  return API[method](apiName, path, init)
    .then(response => {
      if (response.error) {
        throw new Error(`${response.error.code}: ${response.error.message}`)
      }
      return response
    })
}

const authFunction = function(effect: AuthEffect) {
  switch(effect.authAction) {
  case 'changePassword':
    const { oldPassword, newPassword } = effect.payload
    return Auth.currentAuthenticatedUser()
      .then(user => Auth.changePassword(user, oldPassword, newPassword))
  default:
    logger.error('AuthEffect', effect)
    throw new Error(`authAction ${effect.authAction} not implemented!`)
  }
}

export default function(effect: Effects) {
  switch(effect.type) {
  case 'API_EFFECT':
    return apiFunction(effect)
  case 'AUTH_EFFECT':
    return authFunction(effect)
  default:
    throw new Error(`Unhandled effect ${JSON.stringify(effect)}`)
  }
}
