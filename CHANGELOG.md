# 0.1.3
- Tasks appear in alphabetical order
- Added ability to delete tasks without a key

# 0.1.2
- Added ability to filter tasks by free text
- Added ability to reload tasks

# 0.1.1
- Bugfixes

# 0.1.0 
- Initial release
