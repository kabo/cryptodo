# CrypToDo

Zero-knowledge, encrypted todo-list. [https://cryptodo.io/](https://cryptodo.io/)

## Run it locally

```shell
git clone https://gitlab.com/kabo/cryptodo.git
cd cryptodo
yarn install
yarn start
```

## License

Released under [COIL 0.5](./LICENSE.md) ([Copyfree Open Innovation License](http://coil.apotheon.org/)).
